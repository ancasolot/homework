const koa = require("koa");
const koaRouter = require("koa-router");
const koaBody = require("koa-body");

import axios from "axios";

const app = new koa();
const router = new koaRouter();

router.get("/", (ctx) => {
  ctx.body = "Welcome! To the Koala Book of Everything!";
});
router.get("/ping", (ctx) => {
  ctx.body = "pong"
});
router.post("/echo", (ctx) => {
  console.log(ctx.request.body);
  ctx.body = JSON.stringify(ctx.request.body);
});

async function middleWare1() {
  let url = "https://imfine.go.ro/api/app-content";       
  let response = await axios.get(url);

  return response.data;
};

async function middleWare2() {
  let urlNew = 'https://imfine.go.ro/api/psychometrics';
  let response = await axios.get(urlNew);
  
  return response.data;
};

async function getData() {
  return await Promise.all([middleWare1(), middleWare2()]);
};

router.get("/api", async (ctx) => {
  let result = await getData();
    
  ctx.body = result;
  return result;
});

interface Content {
  path: string;
  method: string;
  allowAnonymous: boolean;
  isExternal: boolean;
  resourceId: string;  
  permissions: string[];
};

async function getUsers(): Promise<Content[]> {
  return (await getData()) as Content[];
};

router.get("/work", async (ctx) => {
  let result = await getUsers();
      
  ctx.body = result;
  return result;
});   

app.use(koaBody());
app.use(router.routes()).use(router.allowedMethods());
app.listen(1234);